package eca;

class Utilities {

	final MAVEN_SETTING = '/home/mimente/Maven3/conf/settings.xml'
	final INPUT_DATE = '/home/mimente/mimenteSelfProject/EcaInputDate.java'
	boolean MAVEN_OFFLINE_MODE = false
	final EMAIL_DEV = 'e.bouhdadi@eca-assurances.com'
	final EMAIL_ADMIN = 'e.bouhdadi@eca-assurances.com, a.aitahmed@eca-assurances.com, m.aitahmed@eca-assurances.com, a.essadouni@eca-assurances.com, c.dardillac@eca-assurances.com, c.moulier@eca-assurances.com, s.mancel@eca-assurances.com'
	final IGUANE_IP = 'eca-lb-01.ig-1.net'
	final DOCKER_IMAGE_PREFIX = 'mimente-self'
	
	
	def steps
	Utilities(steps) {this.steps = steps}
	
	def runMaven(project, goals){
		if(project != 'mimente')
			steps.sh "cp $INPUT_DATE src/main/java/com/eca/foundation/gui/component/"
		steps.withMaven(maven: 'Maven3', mavenSettingsFilePath: "$MAVEN_SETTING") { 
				steps.sh "mvn -T 1C $goals" 
			}
	}
	
	
	def upload(host, port, privateKey, file) {
		steps.sh(script:"scp -i $privateKey -p -P $port $file $host", returnStdout: true)
	}
	
	
	def deployOnDocker(project, String[] urls, String port, image, emails){
		steps.node('psql'){
		checkoutFromSCM("svn://192.168.100.10/Docker-deploy-scripts");

		def container;
		def pid = steps.sh (script: 'sudo docker ps -a |  grep \''+port+'\' | awk \'{print $1}\' | tail -1', returnStdout: true);
		if(pid != '')
			steps.sh 'sudo docker stop '+pid+'';
		try{
			def database = steps.sh (script: ' sudo -u postgres psql -c "SELECT datname FROM pg_database order by datname;" | grep \'mimenteProd\' | tail -1', returnStdout: true)
							.toString().trim();
			steps.print "Récupération du nom de la dernière base de donnée importée ==> "+database ;
			container = steps.docker.build("$image:${steps.env.BUILD_ID}","--build-arg project="+project+" --build-arg database="+database+" .")
									.run("--rm -p "+port+":8080");
			steps.print "Démarrage du conteneur"
			steps.timeout( time: 3, unit: "MINUTES" ){
				steps.waitUntil {
						try {
							def r = steps.sh script: 'wget --no-check-certificate -q '+urls[0]+' -O /dev/null', returnStatus: true
						return (r == 0);
						} catch(err) {
							return false;
						}
					}
			}

			sendReportEmail(emails,"INFO: Le déploiement a réussit sur l'environnement pre-prod pour "+project,
								'L\'environnement pre-prod est prét pour recevoir les tests depuis ces liens: <br>'+
								'- <a href="'+urls[0]+'">Espace commercial</a><br>'+
								'- <a href="'+urls[1]+'">Self assurances</a>'+
								'<br><br>La base de donnée utilisée est: 192.168.100.9/<b>'+database+'</b>');

		} catch(err) {
			steps.print err
			"Impossible de créer un environement pre-prod";
		} finally {
			print 'Nettoyage'
			steps.sh 'rm -rf *';
			steps.input message: "Veuillez confirmer que tout est bon sur la Pre-Prod", ok: 'Déployer la version Prod', submitter: 'ebouhdadi,maitahmed,amankari,aaitahmed'
			print "Arrêt du conteneur Docker...";
			container.stop();
			sendReportEmail(emails,"INFO: L'environnement pre-prod "+project+" n'est plus disponible",'L\'environnement pre-prod pour '+project+' a été détruit.');
		}
		}
	}
	
	def startupNotification(instance, url, emails){
	
				try{
						steps.print "En attente le démarrage de Tomcat $instance"
						steps.waitUntil {
									 def r = steps.sh script: "wget -q $url -O /dev/null", returnStatus: true
									 return (r == 0);
								}
						sendReportEmail(emails,"INFO: Le déploiement a réussit sur $instance","La version a été déployée avec succès sur <a href=$url>$instance</a>")
						steps.input message:  "Veuillez Confirmer que la version a été déployée avec succes sur $instance", ok: 'Oui'  
					} catch (error) {
						 print "RollBack to the previous version of BackOfficeSelf"
				}
	}
	
	def sendReportEmail(destination,subject,body){
                steps.emailext mimeType: "text/html", 
                body: 'Bonjour, <br> <br> '+body+' <br> <br> Cordialament, <br> <br> Support Mimente', 
                subject: subject , 
				to: destination
                //to: (destination == 'ADMIN')?EMAIL_ADMIN:EMAIL_DEV
                }
				
	def sendReportEmailWihAttachment(destination,subject,body,file){
		steps.emailext mimeType: "text/html", 
			attachmentsPattern: file,
			body: 'Bonjour, <br> <br> '+body+' <br> <br> Cordialament, <br> <br> Support Mimente', 
			subject: subject , 
			to: destination
			//to: (destination == 'ADMIN')?EMAIL_ADMIN:EMAIL_DEV
	}			
				
				
	def prodDeploy(String project,String instance, String server, String action, String skip, String md5_file){
	
	if( skip == "true" ) {
		steps.input message: "Déployer la version Prod pour "+instance+" ?", ok: 'Oui', submitter: 'ebouhdadi,maitahmed,amankari,aaitahmed'
	}
		
    steps.sshagent (credentials: ['Iguane-key']) {
        def md5 = steps.sh (script: "cat $md5_file", returnStdout: true)
        steps.sh ( script:"""
                ssh -o StrictHostKeyChecking=no customer@$IGUANE_IP << EOF
                ssh -A """+server+"""
                sudo -u """+instance+""" -i
                
                
                
                if [ ! -f /tmp/ """+project+""".war  ]; then
                        echo  '"""+project+""".war for """+instance+""" does not exist!'
                        exit 2
                fi
                
                
                md5=`openssl dgst -md5 /tmp/"""+project+""".war | cut -f 2 -d " "`
                
                if [ ! \$md5 = """+md5+""" ]
                then
                    echo "the generated MD5 does not match!"
                    exit 2
                fi
                
                if [ -f ~/tomcat/"""+instance+""".CATALINA_PID ]
                            then
                            cat ~/tomcat/"""+instance+""".CATALINA_PID | xargs kill -9
                else
                            echo 'no java PID file found for """+instance+"""'
                            exit 2
                fi
                sleep 10s
                
                procjava2="ps aux | grep java | grep """+instance+"""' | grep jdk | tr -s ' ' ' ' | cut -d ' ' -f 2"
                if [ -f \${procjava2}  ]
                then
                    echo "Tomcat been stopped"
                else
                    echo "Tomcat is always running"
                    exit 2
                fi
                
                mv  ~/tomcat/webapps/"""+project+""".war ~/tomcat/oldwars/"""+project+""".war\$(date +%F-%H_%M_%S)
                mv  ~/tomcat/webapps/"""+project+"""/ ~/tomcat/oldwars/"""+project+"""\$(date +%F-%H_%M_%S)
                mv ~/tomcat/logs/catalina.out ~/tomcat/logs/catalina.out\$(date +%F-%H_%M_%S)
                
                                
                rm ~/tomcat/work/Catalina/localhost -rf
                rm ~/tomcat/webapps/"""+project+"""* -rf
				rm ~/tomcat/webapps/ROOT -rf
                
                cp /tmp/"""+project+""".war ~/tomcat/webapps/
                touch ~/tomcat/logs/catalina.out
                
                
                if [ """+action+""" == "start" ] ; then
                    ~/tomcat/startup.sh
                fi
                
                
                
                
            """)
    }
}	

def createNewTag(repoSource, repoTag, build_number){
			try {
				   def pom = steps.readMavenPom file: 'pom.xml'
				   def version = pom.version
				   steps.input message:  "Voulez vous créer un TAG pour ce build ?", ok: "Oui, Créer la version "+version
				   steps.sh "svn copy $repoSource $repoTag/tag-$version -m \"Creating TAG $version  / Jenkins build N°$build_number \""
				   steps.currentBuild.description+="\n"
				   steps.currentBuild.description+= "Une nouvelle tag-$version a été crée."
			   } catch (err) {
					steps.print err
					steps.print 'Pas de TAG pour ce build'
					steps.currentBuild.result = "SUCCESS"
				   }
				   
	}

	
	def checkoutFromSCM(projectUrl) {
			steps.checkout([$class: 'SubversionSCM',
				additionalCredentials: [],
				excludedCommitMessages: '',
				excludedRegions: '',
				excludedRevprop: '',
				excludedUsers: '',
				filterChangelog: false,
				ignoreDirPropChanges: false,
				includedRegions: '',
				locations: [[credentialsId: "SVN",
						depthOption: 'infinity',
						ignoreExternalsOption: false,
						local: '.',
						remote: projectUrl]],
				quietOperation: false,
				workspaceUpdater: [$class: 'UpdateUpdater']])
	}

	def runSonarGates(){
		steps.withMaven(maven: 'Maven3', mavenSettingsFilePath: "$MAVEN_SETTING") { 
				steps.sh "mvn sonar:sonar" 
			}
	}

	def runVulnerabilityTests(){
	
		steps.dependencyCheckAnalyzer datadir: './target/', 
                        hintsFile: '', 
                        includeCsvReports: true, 
                        includeHtmlReports: true, 
                        includeJsonReports: false, 
                        includeVulnReports: true, 
                        isAutoupdateDisabled: false, 
                        outdir: './target/', scanpath: './target/', 
                        skipOnScmChange: false, 
                        skipOnUpstreamChange: false, 
                        suppressionFile: '', 
                        zipExtensions: ''
                        
        steps.dependencyCheckPublisher canComputeNew: false, 
                        defaultEncoding: '', 
                        healthy: '', 
                        pattern: 'target/dependency-check-report.xml', 
                        unHealthy: ''
}

def startZAproxy(name,target){
	steps.sh "docker ps -f name=$name -q | xargs --no-run-if-empty docker container stop"

	steps.sh "docker run -u 0 -it --rm --name $name -v $target/:/zap/wrk/:rw -d owasp/zap2docker-weekly "+
		"zap.sh -daemon -host 127.0.0.1 -config api.disablekey=true -config scanner.attackOnStart=true "+
		"-config view.mode=attack -config connection.dnsTtlSuccessfulQueries=-1 -config api.addrs.addr.name=.* -config api.addrs.addr.regex=true"
	steps.sh "docker exec $name zap-cli status -t 120"
}
def runQuickScanTests(zaproxy, website){
	steps.sh "docker exec $zaproxy zap-cli open-url $website"
    steps.sh "docker exec $zaproxy zap-cli quick-scan --spider -r $website"			
}
def runActiveScanTests(zaproxy, website){
	steps.sh "docker exec $zaproxy zap-cli open-url $website"
    steps.sh "docker exec $zaproxy zap-cli spider $website"
    steps.sh "docker exec $zaproxy zap-cli active-scan -r $website"			
}

def generatePentestReport(zaproxy,report){
    steps.sh "docker exec $zaproxy zap-cli report -o /zap/wrk/$report -f html"
}

def runBaselineTests(website,report_name, report_location){
		steps.sh " docker run -u 0 -i --rm -v $report_location:/zap/wrk/:rw owasp/zap2docker-weekly zap-baseline.py -a -m 5 -r ${report_name}.html -d -t $website"
	}
}